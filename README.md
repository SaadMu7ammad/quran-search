## QuranSearch App

Find verses, words and chapter using the [Quran API Documentation](https://alquran.cloud/api) API

## Project Specifications

- Display UI with Surah input
- Fetch verses and put in DOM
- Add show verse button

  [check it out](https://quran-search-saadmu7ammad-5a6cb19ff082bc1d5338284c9fc7bb2f8500a.gitlab.io/)